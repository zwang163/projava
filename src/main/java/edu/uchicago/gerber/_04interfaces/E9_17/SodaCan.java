package edu.uchicago.gerber._04interfaces.E9_17;

public class SodaCan implements Measurable{
    private double height;
    private double radius;

    //Constructor
    public SodaCan(double h,double r){
        height=h;
        radius=r;
    }

    //get method of surface area
    public double getMeasure(){
        return 2*Math.PI*radius*radius+2*Math.PI*radius*height;
    }
}
