package edu.uchicago.gerber._04interfaces.E9_17;

public class Driver {
    public static void main(String[] args) {
        Measurable[] measurables={new SodaCan(1,2),new SodaCan(2,3),new SodaCan(3,4)};
        double total=0;
        for (Measurable m:measurables){
            total+=m.getMeasure();
        }
        double average=total/measurables.length;
        System.out.println("The average is: "+average);
    }
}
