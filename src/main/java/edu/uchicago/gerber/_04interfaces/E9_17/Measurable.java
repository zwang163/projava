package edu.uchicago.gerber._04interfaces.E9_17;

public interface Measurable {
    public double getMeasure();
}
