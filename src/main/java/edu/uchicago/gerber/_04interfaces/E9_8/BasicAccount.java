package edu.uchicago.gerber._04interfaces.E9_8;

public class BasicAccount extends BankAccount{
    @Override
    public void withdraw(double amount) {
        super.withdraw(amount);
        if (balance<0) balance=0;
    }
}
