package edu.uchicago.gerber._04interfaces.E9_8;

public class BankAccount {
    protected double balance;
    public void deposit(double amount){
        balance += amount;
    }
    public void withdraw(double amount){
        balance-=amount;
    }
    public void monthEnd(){

    }
    public double getBalance(){
        return balance;
    }
}
