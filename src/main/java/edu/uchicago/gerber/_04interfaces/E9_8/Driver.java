package edu.uchicago.gerber._04interfaces.E9_8;

//driver class for test
public class Driver {
    public static void main(String[] args) {
        BankAccount bankAccount=new BasicAccount();
        bankAccount.deposit(100);
        System.out.println(bankAccount.getBalance());
        bankAccount.withdraw(50);
        System.out.println(bankAccount.getBalance());
        bankAccount.withdraw(200);
        System.out.println(bankAccount.getBalance());
    }
}
