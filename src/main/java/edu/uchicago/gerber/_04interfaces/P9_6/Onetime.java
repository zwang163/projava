package edu.uchicago.gerber._04interfaces.P9_6;

public class Onetime extends Appointment{
    public Onetime(String description,int year,int month,int day){
        super(description,year,month,day);
    }
}
