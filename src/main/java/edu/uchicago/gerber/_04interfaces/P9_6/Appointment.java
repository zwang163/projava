package edu.uchicago.gerber._04interfaces.P9_6;

import java.util.Date;

public class Appointment {
    protected String description;
    //to implement subclasses, I will take 0 as "casual", for example, 0 year means the appointment happens every year.
    protected int year,month,day;
    public Appointment(String description,int year,int month,int day){
        this.description=description;
        this.year=year;
        this.month=month;
        this.day=day;
    }
    public boolean occursOn(int year,int month,int day){
        if ((this.year!=0&&this.year!=year)
                ||(this.month!=0&&this.month!=month)
                ||(this.day!=0&&this.day!=day)) return false;
        return true;
    }
    public String getDescription(){
        return description;
    }
}
