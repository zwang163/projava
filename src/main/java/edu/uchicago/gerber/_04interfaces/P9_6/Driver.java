package edu.uchicago.gerber._04interfaces.P9_6;

import java.util.Scanner;

//Driver for test
public class Driver {
    public static void main(String[] args) {
        Appointment[] appointments={new Daily("Have dinner"),new Monthly("Take a test",1),new Onetime("Graduate",2021,1,1),
                                new Daily("Sleep"),new Monthly("Shop",15)};
        Scanner scanner=new Scanner(System.in);
        System.out.print("Please enter a year, a month, and a day, separated by a space: ");
        int year=scanner.nextInt(),month=scanner.nextInt(),day=scanner.nextInt();
        System.out.println("Appointments are:");
        for (Appointment appointment:appointments){
            if (appointment.occursOn(year,month,day)){
                System.out.println(appointment.getDescription());
            }
        }
    }
}
