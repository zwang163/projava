package edu.uchicago.gerber._04interfaces.P9_1;

public class Clock {
    protected int minutes, hours;
    public Clock(){
        minutes= java.time.LocalTime.now().getMinute();
        hours=java.time.LocalTime.now().getHour();
    }

    public int getHours() {
        return hours;
    }
    public int getMinutes(){
        return  minutes;
    }
    public String getTime(){
        return "Hours: "+getHours()+". Minutes: "+getMinutes()+".";
    }
}
