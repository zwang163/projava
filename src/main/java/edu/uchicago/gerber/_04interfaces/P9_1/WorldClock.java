package edu.uchicago.gerber._04interfaces.P9_1;

public class WorldClock extends Clock{
    int offset;
    public WorldClock(int offset) {
        super();
        this.offset=offset;
    }
    @Override
    public int getHours() {
        return hours + offset;
    }
}
