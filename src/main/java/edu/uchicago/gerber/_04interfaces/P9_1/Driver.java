package edu.uchicago.gerber._04interfaces.P9_1;

//driver for test
public class Driver {
    public static void main(String[] args) {
        Clock clock=new Clock();
        System.out.println(clock.getTime());
        Clock worldClock=new WorldClock(1);
        System.out.println(worldClock.getTime());
    }
}
