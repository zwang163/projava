package edu.uchicago.gerber._04interfaces.E9_13;

import java.awt.*;

//Driver for test
public class Driver {
    public static void main(String[] args) {
        BetterRectangle rectangle=new BetterRectangle(1,1,10,20);
        System.out.println(rectangle.getPerimeter());
        System.out.println(rectangle.getArea());
    }
}
