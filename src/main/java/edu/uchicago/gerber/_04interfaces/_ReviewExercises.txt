#########################################################################
# Use this file to answer Review Exercises from the Big Java textbook
#########################################################################

R9.1 Superclas and subclass
a.
    superclass: Employee
    subclass: Manager
b.
    superclass: Student
    subclass: GraduateStudent
c.
    superclass: Person
    subclass: Student
d.
    superclass: Employee
    subclass: Professor
e.
    superclass: BankAccount
    subclass: CheckingAccount
f.
    superclass: Vehicle
    subclass: Car
g.
    superclass: Vehicle
    subclass: Minivan
h.
    superclass: Car
    subclass: Minivan
i.
    superclass: Vehicle
    subclass: Truck

R9.2 superclass and subclass
Because they share almost no common thing that need a super class SmallAppliance.

R9.4 SavingsAccount
It inherit deposit(double amount) and getBalance().
It override withdraw(double amount) and monthEnd().
In addition, it need to record the number of withdraws.

R9.6 Sandwich
a and d are legal.


*****************************
For question R9.7, R9.8, andR9.9 I don't know why I cannot push the uml file, I could only push the source code which used to generate the uml files
*****************************

R9.7 Inheretence -use the UML diagram tool in IntelliJ and indicate to the grader where uml file is located
Please see floder R9_7 under floder _04interfaces

R9.8 Inheretence -use the UML diagram tool in IntelliJ and indicate to the grader where uml file is located
Please see floder R9_8 under floder _04interfaces

R9.9 Inheretence -use the UML diagram tool in IntelliJ and indicate to the grader where uml file is located
Please see floder R9_9 under floder _04interfaces

R9.10 Casting
The returned type is different.
(BankAccount) x will return an object of BankAccount.
(int) x will return an integer.


R9.11 instanceof operator
a. true
b. true
c. false
d. true
e. true
f. false

R9.14 Edible interface
a, c, d, and f are legal