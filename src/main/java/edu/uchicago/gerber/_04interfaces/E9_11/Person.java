package edu.uchicago.gerber._04interfaces.E9_11;

public class Person {
    protected String name;
    protected int birthYear;
    public Person(String name,int birthYear){
        this.name = name;
        this.birthYear = birthYear;
    }
    public String toString(){
        return ("Name of person: "+name+
                "\nYear of birth: "+birthYear);
    }
}
