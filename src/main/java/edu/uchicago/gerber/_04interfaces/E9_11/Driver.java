package edu.uchicago.gerber._04interfaces.E9_11;
//driver class for test
public class Driver {
    public static void main(String[] args) {
        Person[] people={new Person("person",1980),new Instructor("instructor",1981,10000),new Student("student",1990,"Computer Science")};
        for (Person p:people){
            System.out.println(p);
        }
    }
}
