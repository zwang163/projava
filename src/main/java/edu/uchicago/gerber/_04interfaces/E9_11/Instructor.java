package edu.uchicago.gerber._04interfaces.E9_11;

public class Instructor extends Person{
    private double salary;
    public Instructor(String name,int birthYear,double salary){
        super(name,birthYear);
        this.salary=salary;
    }
    @Override
    public String toString(){
        return ("Name of instructor: "+name+
                "\nYear of birth: "+birthYear+
                "\nSalary: "+salary);
    }

}
