package edu.uchicago.gerber._04interfaces.E9_11;

public class Student extends Person{
    private String major;
    public Student(String name,int birthYear,String major){
        super(name,birthYear);
        this.major=major;
    }
    @Override
    public String toString(){
        return ("Name of student: "+name+
                "\nYear of birth: "+birthYear+
                "\nMajor: "+major);
    }
}
