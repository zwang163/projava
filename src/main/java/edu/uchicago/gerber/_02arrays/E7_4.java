package edu.uchicago.gerber._02arrays;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

public class E7_4 {
    public static void main(String[] args) throws Exception{
        Scanner usr=new Scanner(System.in);
        System.out.print("Please enter the input file name: ");
        Scanner in=new Scanner(new File(usr.nextLine()));
        System.out.print("Please enter the output file name: ");
        PrintWriter out = new PrintWriter(usr.nextLine());
        int line_num=1;
        while (in.hasNext()){
            String str="/* "+line_num+" */ "+in.nextLine();
            out.print(str);
            if (in.hasNext()) out.println();
            line_num++;
        }
        in.close();
        out.close();
    }
}
