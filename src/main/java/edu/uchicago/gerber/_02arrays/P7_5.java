package edu.uchicago.gerber._02arrays;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

public class P7_5 {
    //main for test
    public static void main(String[] args) throws Exception{
        //System.out.println(numberOfRows());
        //System.out.println(numberOfFields(1));
        System.out.println(field(1,1));
    }
    public static int numberOfRows() throws Exception{
        Scanner usr=new Scanner(System.in);
        System.out.print("Please enter the input file name: ");
        Scanner in=new Scanner(new File(usr.nextLine()));
        int row=0;
        while (in.hasNext()){
            in.nextLine();
            row++;
        }
        in.close();
        return row;
    }
    public static int numberOfFields(int row) throws Exception{
        Scanner usr=new Scanner(System.in);
        System.out.print("Please enter the input file name: ");
        Scanner in=new Scanner(new File(usr.nextLine()));
        for(int i=1;i<row;i++){
            in.nextLine();
        }
        int field=1;
        String str=in.nextLine();
        for(int i=0;i<str.length();i++){
            if (str.charAt(i)==',') field++;
        }
        in.close();
        return field;
    }
    public static String field(int row,int column) throws Exception{
        Scanner usr=new Scanner(System.in);
        System.out.print("Please enter the input file name: ");
        Scanner in=new Scanner(new File(usr.nextLine()));
        for(int i=1;i<row;i++){
            in.nextLine();
        }
        int field=1;
        String str=in.nextLine();
        String ans="";
        for(int i=0;i<str.length();i++){
            if (str.charAt(i)==',') field++;
            if (field==column) ans+=str.charAt(i);
        }
        return ans;
    }
}
