package edu.uchicago.gerber._02arrays;

import java.util.Scanner;

public class P5_24 {
    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        System.out.print("Please enter a Roman number: ");
        String str=s.nextLine();
        int total = 0;
        while (!str.equals("")){
            if (str.length()==1||(value(str.charAt(0))>=value(str.charAt(1)))){
                total+=value(str.charAt(0));
                str=str.substring(1);
            }
            else {
                int diff= value(str.charAt(1))-value(str.charAt(0));
                total+=diff;
                str=str.substring(2);

            }

        }
        System.out.print("The decimal representation is: " + total);
    }
    public static int value(char c) {
        if (c=='I') return 1;
        if (c=='V') return 5;
        if (c=='X') return 10;
        if (c=='L') return 50;
        if (c=='C') return 100;
        if (c=='D') return 500;
        if (c=='M') return 1000;
        return 0;
    }
}
