package edu.uchicago.gerber._02arrays;

import java.util.*;

public class E6_12 {
    public static void main(String[] args) {
        int[] array=new int[20];
        Random r=new Random();
        for (int i=0;i<array.length;i++){
            array[i]=r.nextInt(100);
        }
        System.out.print("The array is:\n["+array[0]);
        for (int i=1;i<array.length;i++){
            System.out.print(", "+array[i]);
        }
        System.out.println("]");
        Arrays.sort(array);
        System.out.print("The sorted array is:\n["+array[0]);
        for (int i=1;i<array.length;i++){
            System.out.print(", "+array[i]);
        }
        System.out.println("]");
    }
}
