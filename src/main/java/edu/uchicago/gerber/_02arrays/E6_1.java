package edu.uchicago.gerber._02arrays;

import java.util.Random;

public class E6_1 {
    public static void main(String[] args) {
        int[] array=new int[10];
        Random a=new Random();
        for (int i=0;i<array.length;i++){
            array[i]=a.nextInt();
        }

        System.out.println("Every element at an even index:");
        for (int i=0;i<array.length;i+=2){
            System.out.print(array[i]+" ");
        }
        System.out.println();
        System.out.println("Every even element:");
        for (int i=0;i<array.length;i++){
            if (array[i]%2==0) System.out.print(array[i]+" ");
        }
        System.out.println();
        System.out.println("All elements in reverse order:");
        for (int i=array.length-1;i>=0;i--){
            System.out.print(array[i]+" ");
        }
        System.out.println();
        System.out.println("Only the first and last element:");
        System.out.println(array[0]+ "  "+array[array.length-1]);

    }
}
