package edu.uchicago.gerber._02arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class E6_16 {
    public static void main(String[] args) {
        System.out.print("Please enter a sequence of values separated by a space: ");
        Scanner s=new Scanner(new Scanner(System.in).nextLine());
        double max=0;
        ArrayList<Double> arr=new ArrayList<Double>();
        while (s.hasNext()){
            double tmp= s.nextDouble();
            if (tmp>max) max=tmp;
            arr.add(tmp);
        }
        for (int i=0;i<40;i++){
            for (int j=0;j<arr.size();j++){
                if (1-arr.get(j)/max<=i/40.0) System.out.print("*");
                else System.out.print(" ");
            }
            System.out.println();
        }
    }
}
