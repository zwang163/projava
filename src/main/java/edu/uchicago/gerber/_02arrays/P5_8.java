package edu.uchicago.gerber._02arrays;

import java.util.Scanner;

public class P5_8 {
    //main for test
    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        System.out.print("Please enter a year: ");
        int a=s.nextInt();
        if (isLeapYear(a)) System.out.print("It is a leap year");
        else  System.out.print("It is not a leap year");
    }

    // the function is the answer
    public static boolean isLeapYear(int year) {
        if (year%400==0) return true;
        if (year%100==0) return false;
        if (year%4==0) return true;
        return false;
    }
}
