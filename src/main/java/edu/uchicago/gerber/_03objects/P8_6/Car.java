package edu.uchicago.gerber._03objects.P8_6;

public class Car {
    private double efficiency;
    private double gas;

    //Consor
    public Car(double e){
        efficiency=e;
    }

    //methods required
    public void addGas(double g){
        gas+=g;
    }

    public void drive(double distance){
        gas-=distance/efficiency;
    }

    public double getGasLevel(){
        return gas;
    }
}
