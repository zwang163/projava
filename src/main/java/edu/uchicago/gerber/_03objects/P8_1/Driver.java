package edu.uchicago.gerber._03objects.P8_1;

//Driver for test
public class Driver {
    public static void main(String[] args) {
        Microwave microwave=new Microwave();
        microwave.start();
        microwave.increase_time(300);
        microwave.switching();
        microwave.start();
    }
}
