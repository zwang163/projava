package edu.uchicago.gerber._03objects.P8_1;

public class Microwave {
    private int seconds;
    private int level = 1;

    //button for increasing the time
    public void increase_time(int sec){
        seconds+=sec;
    }

    //button for switching
    public void switching(){
        level= level==2?1:2;
    }

    //reset button
    public void rest(int sec){
        seconds=0;
        level=1;
    }

    //start button
    public void start(){
        System.out.println("Cooking for "+seconds+" seconds at level "+level+".");
    }

}
