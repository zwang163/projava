package edu.uchicago.gerber._03objects.P8_7;

//Driver class for test
public class Driver {
    public static void main(String[] args) {
        ComboLock comboLock=new ComboLock(1,38,0);
        System.out.println("The combo lock can be open? "+comboLock.open());
        comboLock.turnRight(1);
        comboLock.turnLeft(3);
        comboLock.turnRight(1);
        comboLock.turnRight(1);
        System.out.println("The combo lock can be open? "+comboLock.open());
        comboLock.turnRight(1);
        System.out.println("The combo lock can be open? "+comboLock.open());
        comboLock.reset();
        comboLock.turnRight(1);
        comboLock.turnLeft(3);
        comboLock.turnRight(2);
        System.out.println("The combo lock can be open? "+comboLock.open());
    }
}
