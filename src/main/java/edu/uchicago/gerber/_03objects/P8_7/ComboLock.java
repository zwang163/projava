package edu.uchicago.gerber._03objects.P8_7;

public class ComboLock {
    private int secret1,secret2,secret3;
    //num to record user's input
    private int num1=-1,num2=-1;
    //cur to record position, consider right as positive
    private int cur;

    public ComboLock(int secret1,int secret2, int secret3){
        this.secret1=secret1;
        this.secret2=secret2;
        this.secret3=secret3;
    }

    public void reset(){
        num1=-1;
        num2=-1;
        cur=0;
    }

    public void turnLeft(int ticks){
        if (num1==-1){
            num1=cur;
        }
        //change cur according to ticks
        cur=(cur-ticks)%40;
        if (cur<0) cur+=40;
    }

    public void turnRight(int ticks){
        if (num1!=-1&&num2==-1){
            num2=cur;
        }
        cur=(cur+ticks)%40;
    }

    public  boolean open(){
        if (num1==secret1&&num2==secret2&&cur==secret3) return true;
        return false;
    }



}
