package edu.uchicago.gerber._03objects.P8_14;

public class Country {
    String name;
    int population;
    double area;

    public Country(String n,int p,double a){
        name=n;
        population=p;
        area=a;
    }
}
