package edu.uchicago.gerber._03objects.P8_14;

public class Driver {
    public static void main(String[] args) {
        Country[] countries={new Country("country1",1000,10000.0),new Country("country2",5000,9000.0),new Country("country3",500,500.0)};
        report(countries);
    }

    //function that reads a set of countries and prints required info
    public static void report(Country[] countries){
        int population_max=0;
        double area_max=0.0;
        double density_max=0.0;
        String country_population_max="",country_area_max="",country_density_max="";
        for (Country c:countries){
            if (c.population>population_max) {
                country_population_max=c.name;
                population_max=c.population;
            }
            if (c.area>area_max) {
                country_area_max=c.name;
                area_max=c.area;
            }
            double density=c.population/c.area;
            if (density>density_max) {
                country_density_max=c.name;
                density_max=density;
            }
        }

        System.out.println("The country with the largest area: " +country_area_max+"\n"+
                "The country with the largest population: "+country_population_max+"\n"+
                "The country with the largest population density: "+country_density_max);
    }
}
