package edu.uchicago.gerber._03objects.P8_16;

//implement of Message Class
public class Message {
    private String sender,recipient,text="";

    public Message(String sender,String recipient){
        this.sender=sender;
        this.recipient=recipient;
    }

    public void append(String text){
        if (this.text.equals("")){
            this.text=text;
            return;
        }
        this.text+="\n"+text;
    }

    public String toString(){
        return text.replace("\n","%n");
    }

}
