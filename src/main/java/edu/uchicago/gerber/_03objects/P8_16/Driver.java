package edu.uchicago.gerber._03objects.P8_16;


//Driver for test
public class Driver {
    public static void main(String[] args) {
        Message message=new Message("a","b");
        message.append("text1");
        message.append("text2");
        System.out.println(message);
        Mailbox mailbox=new Mailbox();
        mailbox.addMessage(message);
        System.out.print(mailbox.messages.size()+": ");
        System.out.println(mailbox.getMessage(0));
        mailbox.removeMessage(0);
        System.out.println(mailbox.messages.size());

    }
}
