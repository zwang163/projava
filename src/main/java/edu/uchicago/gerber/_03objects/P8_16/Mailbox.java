package edu.uchicago.gerber._03objects.P8_16;

import java.util.ArrayList;

//implement of Mailbox Class
public class Mailbox {
    ArrayList<Message> messages=new ArrayList<Message>();
    public void addMessage(Message m){
        messages.add(m);
    }
    public  Message getMessage(int i){
        return messages.get(i);
    }
    public  void removeMessage(int i){
        messages.remove(i);
    }

}
