package edu.uchicago.gerber._03objects.P8_8;

public class Driver {
    public static void main(String[] args) {
        VotingMachine v=new VotingMachine();
        v.voteDemocrat();
        v.voteRepublican();
        v.voteRepublican();
        System.out.println("Democrat: "+v.getDemocrat());
        System.out.println("Republican: "+v.getRepublican());
    }
}
