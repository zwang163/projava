package edu.uchicago.gerber._03objects.P8_8;

public class VotingMachine {
    private int democrat,republican;
    public void voteDemocrat(){
        democrat++;
    }
    public void voteRepublican(){
        republican++;
    }

    public int getDemocrat() {
        return democrat;
    }

    public int getRepublican() {
        return republican;
    }
}
