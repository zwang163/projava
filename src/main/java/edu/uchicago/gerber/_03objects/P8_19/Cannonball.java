package edu.uchicago.gerber._03objects.P8_19;

import edu.uchicago.gerber._03objects.P8_16.Message;


//this is required Cannonball class
public class Cannonball {
    private double position_x,position_y;
    double velocity_x,velocity_y;
    public Cannonball(double x){
        this.position_x=x;
    }
    public void move(double sec){
        position_x+=sec*velocity_x;
        position_y+=velocity_y*sec+1.0/2*(-9.81)*sec*sec;
        velocity_y+=(-9.81)*sec;
    }
    public double getX(){
        return position_x;
    }
    public double getY(){
        return position_y;
    }
    public void shoot(double a,double v){
        a=Math.toRadians(a);
        velocity_x=v*Math.cos(a);
        velocity_y=v*Math.sin(a);
        for (double t=0;position_y>=0;move(0.1)){
            System.out.println("The position is: \nx-position = "+getX()+", y-position = "+getY());
        }
    }
}
