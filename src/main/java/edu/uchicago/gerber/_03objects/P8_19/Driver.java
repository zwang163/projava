package edu.uchicago.gerber._03objects.P8_19;


import java.util.Scanner;

//Driver that ask for user input and call shoot
public class Driver {
    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        System.out.print("Please enter a starting angle: ");
        double angle=s.nextDouble();
        System.out.print("Please enter a initial velocity: ");
        double velocity=s.nextDouble();
        Cannonball cannonball=new Cannonball(0);
        cannonball.shoot(angle,velocity);
    }
}
