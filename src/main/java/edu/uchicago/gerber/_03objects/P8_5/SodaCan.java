package edu.uchicago.gerber._03objects.P8_5;

public class SodaCan {
    private double height;
    private double radius;

    //Constructor
    public SodaCan(double h,double r){
        height=h;
        radius=r;
    }

    //get method of surface area
    public double getSurfaceArea(){
        return 2*Math.PI*radius*radius+2*Math.PI*radius*height;
    }

    //get method of volume
    public double getVolume(){
        return Math.PI*radius*radius*height;
    }

}
