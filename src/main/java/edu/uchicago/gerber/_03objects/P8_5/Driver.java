package edu.uchicago.gerber._03objects.P8_5;

public class Driver {
    public static void main(String[] args) {
        SodaCan sodaCan=new SodaCan(100.0,10.0);
        System.out.println("Surface area: "+sodaCan.getSurfaceArea());
        System.out.println("Volume: "+sodaCan.getVolume());
    }
}
