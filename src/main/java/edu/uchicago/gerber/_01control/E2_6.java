package edu.uchicago.gerber._01control;
import java.util.Scanner;

import java.util.Scanner;

public class E2_6 {
    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        System.out.print("Please enter a distance in meters: ");
        double a=s.nextDouble();
        System.out.print("The distance can be expressed as: "+a*0.00062137+" miles, "+a*3.2808399+" feet, or "+a*0.03937008+" inches");
    }
}
