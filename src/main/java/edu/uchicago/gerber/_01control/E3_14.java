package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class E3_14 {
    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        System.out.print("Please enter the month and the day, separated by a space: ");
        int m=s.nextInt(),d=s.nextInt();
        String season="";
        if (m==1||m==2||m==3) season="Winter";
        else if (m==4||m==5||m==6) season="Spring";
        else if (m==7||m==8||m==9) season="Summer";
        else if (m==10||m==11||m==12) season="Fall";
        if (m%3==0&&d>=21)
            if (season=="Winter")season="Spring";
            else if (season=="Spring")season="Summer";
            else if (season=="Summer")season="Fall";
            else season="Winter";
        System.out.print("The season is "+season+".");
    }
}
