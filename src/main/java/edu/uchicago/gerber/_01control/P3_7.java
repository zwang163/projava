package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class P3_7 {
    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        System.out.print("Please enter a income: ");
        double a=s.nextDouble();
        double tax=a*0.01;
        if (a>50000) tax+=0.01*(a-50000);
        if (a>75000) tax+=0.01*(a-75000);
        if (a>100000) tax+=0.01*(a-100000);
        if (a>250000) tax+=0.01*(a-250000);
        if (a>500000) tax+=0.01*(a-500000);
        System.out.print("The tax is : "+tax+".");
    }
}
