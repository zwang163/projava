package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class P3_14 {
    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        System.out.print("Please enter a year: ");
        int a=s.nextInt();
        if (a%400==0) System.out.print("It is a leap year.");
        else if (a%100==0) System.out.print("It is not a leap year.");
        else if (a%4==0) System.out.print("It is a leap year.");
        else System.out.print("It is not a leap year.");
    }
}
