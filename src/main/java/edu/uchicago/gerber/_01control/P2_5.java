package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class P2_5 {
    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        System.out.print("Please enter a price as a floating-point value: ");
        double a=s.nextDouble();
        int dollars=(int)a;
        int cents=(int)((a-dollars)*100+0.5);
        System.out.print("The price is "+dollars+" dollars and "+cents+" cents.");
    }
}
