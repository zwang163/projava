package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class E4_1 {
    public static void main(String[] args) {
        int sum = 0;
        for (int i=2;i<=100;i++){
            if (i%2==0) sum+=i;
        }
        System.out.println("a. "+sum);
        sum=0;
        for (int i=1;i*i<=100;i++){
            sum+=i*i;
        }
        System.out.println("b. "+sum);
        sum=0;
        int pow_2=1;
        for (int i=0;i<=20;i++){
            sum+=pow_2;
            pow_2*=2;
        }
        System.out.println("c. "+sum);
        Scanner s=new Scanner(System.in);
        System.out.print("d. Please enter two inputs separated by a space:");
        int a=s.nextInt(),b=s.nextInt();
        sum=0;
        for (int i=a;i<=b;i++){
            if (i%2==1) sum+=i;
        }
        System.out.println("d. the sum is:"+sum);
        System.out.print("e. Please an input:");
        a=s.nextInt();
        sum=0;
        while (a!=0){
            if (a%10%2==1) sum+=a%10%2;
            a/=10;
        }
        System.out.print("e. the sum is:"+sum);
    }
}
