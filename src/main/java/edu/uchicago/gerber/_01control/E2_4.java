package edu.uchicago.gerber._01control;
import java.util.Scanner;
public class E2_4 {
    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        System.out.print("Please enter two integers separated by a space: ");
        int a=s.nextInt(),b=s.nextInt();
        System.out.print("The sum is:"+(a+b)+
                "\nThe difference is:"+(a-b)+
                "\nThe product is:"+(a*b)+
                "\nThe average is:"+(a+b)/2+
                "\nThe distance is:"+Math.abs(a-b)+
                "\nThe maximum is:"+Math.max(a,b)+
                "\nThe minimum is:"+Math.min(a,b));
    }
}
