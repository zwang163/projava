package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class P3_13 {
    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        System.out.print("Please enter a integer: ");
        String ans="";
        int a=s.nextInt();
        while (a!=0){
            for (int i=a/1000;i>0;i--) ans+="M";
            a%=1000;
            for (int i=a/500;i>0;i--) if (i==4) {ans+="D";break;} else ans+="D";
            a%=500;
            for (int i=a/100;i>0;i--) if (i==4) {ans+="C";break;} else ans+="C";
            a%=100;
            for (int i=a/50;i>0;i--) if (i==4) {ans+="L";break;} else ans+="L";
            a%=50;
            for (int i=a/10;i>0;i--) if (i==4) {ans+="X";break;} else ans+="X";
            a%=10;
            for (int i=a/5;i>0;i--) if (i==4) {ans+="V";break;} else ans+="V";
            a%=5;
            for (int i=a;i>0;i--)  if (i==4) {ans+="I";break;} else ans+="I";
            a%=1;
        }
        System.out.print(ans);
    }
}
